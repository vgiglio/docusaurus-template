// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'EPL Institution Documentation',
  url: 'https://www.enovacom.fr/',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: '/img/icon.svg',
  organizationName: 'enovacom', // Usually your GitHub org/user name.
  projectName: 'epl-institutions', // Usually your repo name.
  i18n: {
    defaultLocale: 'fr',
    locales: ['fr', 'en'],
    localeConfigs: {
      fr: {
        label: 'Français',
      },
      en: {
        label: 'English',
      },
    },
  },
  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // lastVersion: '1.0.0',
          // versions: {
          //   current: {
          //     label: '1.0.0 ',
          //     path: '1.0.0',
          //   },
          // },
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      colorMode: {
        defaultMode: 'light',
        disableSwitch: false,
      },
      hideableSidebar: true,
      navbar: {
        title: 'Accueil',
        logo: {
          alt: 'EPL logo',
          src: 'img/icon.svg',
        },
        items: [
          {
            type: 'doc',
            docId: 'functionnalUnits',
            position: 'left',
            label: 'Documentation',
          },
          // {
          //   type: 'docsVersion',
          //   position: 'left',
          //   label: '1.0.0',
          // },
          // UnComment when translations are added
          // {
          //   type: 'localeDropdown',
          //   position: 'right',
          // },
        ],
      },
      footer: {
        style: 'light',
        copyright: `Copyright © ${new Date().getFullYear()} Enovacom`,
      },
      prism: {
        theme: require('prism-react-renderer/themes/github'),
        darkTheme: require('prism-react-renderer/themes/dracula'),
      },
    }),
  plugins: [require.resolve('docusaurus-lunr-search')],
};

module.exports = config;
