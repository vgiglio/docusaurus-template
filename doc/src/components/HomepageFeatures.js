import React from 'react';
import useBaseUrl from '@docusaurus/useBaseUrl';
import clsx from 'clsx';
import styles from '../css/HomepageFeatures.module.css';
import { v4 as uuidv4 } from 'uuid';

const features = [
  {
    title: 'Consulter la liste des unités fonctionnelles',
    src: 'img/feature1.svg',
    description: (
      <>
        Lorem Ipsum is simply dummy text of the printing and typesetting
        industry. Lorem Ipsum has been the industry's standard dummy text ever
        since the 1500s.
      </>
    ),
  },
  {
    title: "Renseigner les détails de l'établissement",
    src: 'img/feature2.svg',
    description: (
      <>
        Lorem Ipsum is simply dummy text of the printing and typesetting
        industry. Lorem Ipsum has been the industry's standard dummy text ever
        since the 1500s.
      </>
    ),
  },
  {
    title: "Administrer la maintenance de l'établissement",
    src: 'img/feature3.svg',
    description: (
      <>
        Lorem Ipsum is simply dummy text of the printing and typesetting
        industry. Lorem Ipsum has been the industry's standard dummy text ever
        since the 1500s.
      </>
    ),
  },
  {
    title: 'Personnaliser les modèles de notification',
    src: 'img/feature4.svg',
    description: (
      <>
        Lorem Ipsum is simply dummy text of the printing and typesetting
        industry. Lorem Ipsum has been the industry's standard dummy text ever
        since the 1500s.
      </>
    ),
  },
];

function Feature({ src, title, description }) {
  const imgSrc = useBaseUrl(src);
  return (
    <div className={clsx('col col--6')}>
      <div className="text--center">
        <img className={styles.featureImg} src={imgSrc} alt={title} />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section>
      <div className="container">
        <div className="row">
          {features &&
            features.length > 0 &&
            features.map(({ src, title, description }) => (
              <Feature
                key={uuidv4()}
                src={src}
                title={title}
                description={description}
              />
            ))}
        </div>
      </div>
    </section>
  );
}
