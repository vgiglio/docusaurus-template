import React, { useState, useEffect } from 'react';
import Link from '@docusaurus/Link';
import Translate from '@docusaurus/Translate';
import Icon from '@petank-ui/design-tokens/dist/epl/logos/icon.svg'; //usually product Icon

import clsx from 'clsx';
import { useColorMode } from '@docusaurus/theme-common';
import styles from '../css/index.module.css';

export default function HeroBanner({ siteConfig, i18n }) {
  const [pdflanguage, setPdfLanguage] = useState('');
  const languages = i18n.locales;
  const { isDarkTheme } = useColorMode();

  const background = isDarkTheme
    ? styles.heroBannerDark
    : styles.heroBannerLight;

  const waveBackground = isDarkTheme ? styles.waveDark : styles.waveLight;

  useEffect(() => {
    const languagefilter = languages.filter(
      (language) => `/doc/${language}/` === window.location.pathname,
    );
    setPdfLanguage(languagefilter.length ? languagefilter : i18n.defaultLocale);
  }, []);

  return (
    <header>
      <div className={clsx('hero', background)}>
        <div className={clsx('container', styles.container)}>
          <Icon className={styles.logo} />
          <h1 className="hero__title">{siteConfig.title}</h1>
          <p className="hero__subtitle">
            <Translate id="home.subTitle">
              Description de l'application
            </Translate>
          </p>
          <div className={styles.buttons}>
            <Link
              className="button button--outline button--primary button--lg"
              to="docs/functionnalUnits"
            >
              <Translate id="home.seeDoc">Voir la documentation</Translate>
            </Link>
          </div>
        </div>
      </div>
      <div className={styles.waveContainer}>
        <div className={clsx(waveBackground)}></div>
      </div>
    </header>
  );
}
