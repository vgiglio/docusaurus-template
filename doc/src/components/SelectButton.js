import React, { useState, useEffect } from 'react';
import Link from '@docusaurus/Link';
import Translate from '@docusaurus/Translate';
import clsx from 'clsx';

// import versions from '../../versions.json';

import styles from '../css/index.module.css';
import DownArrow from '../../static/img/downArrow.svg';

function SelectButton({ i18n }) {
  const [pdflanguage, setPdfLanguage] = useState(i18n.defaultLocale);
  const languages = i18n.locales;

  useEffect(() => {
    const languagefilter = languages.filter(
      (language) => `/doc/${language}/` === window.location.pathname,
    );
    languagefilter.length && setPdfLanguage(languagefilter);
  }, []);

  return (
    <div class="dropdown dropdown--hoverable">
      <button className={clsx('button button--primary', styles.flexButton)}>
        <Translate className={styles.downloadLink} id="home.downloadDoc">
          Télécharger la documentation
        </Translate>
        <DownArrow className={styles.downloadIcon} />
      </button>
      <ul className={clsx('dropdown__menu', styles.list)}>
        {/* {versions.map((version) => (
          <li>
            <Link
              className="dropdown__link"
              to={`pdf/EPL_${pdflanguage}_doc-test.pdf`}
              target="_blank"
              download
            >
              <p className={styles.downloadLink}>TEST</p>
            </Link>
          </li>
        ))} */}
      </ul>
    </div>
  );
}

export default SelectButton;
