import React, { useEffect, useState } from 'react';
import Layout from '@theme/Layout';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import HomepageFeatures from '../components/HomepageFeatures';
import HeroBanner from '../components/HeroBanner';
// import SelectButton from "../components/SelectButton";

export default function Home() {
  const { siteConfig, i18n } = useDocusaurusContext();

  return (
    <Layout
      title={`Hello from ${siteConfig.title}`}
      description="Description will go into a meta tag in <head />"
    >
      {/* <SelectButton i18n={i18n} /> */}
      <HeroBanner siteConfig={siteConfig} i18n={i18n} />
      <main>
        <HomepageFeatures />
      </main>
    </Layout>
  );
}

{
  /* <div className={styles.downloadContainer}>
          <Link
            
           to={`pdf/EPL_${pdflanguage}_doc-1.0.0.pdf`}
            target="_blank"
            download
          >
            <img
              className={styles.downloadLogo}
              src="img/download-icon.png"
              alt="télécharger pdf"
            />
            <p className={styles.downloadLink}>
              <Translate id="home.downloadDoc">
                Télécharger la documentation
              </Translate>
            </p>
          </Link>
        </div> */
}
