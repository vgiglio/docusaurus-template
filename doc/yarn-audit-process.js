var readline = require('readline');
var fs = require('fs')

const targetFolder = './target'

if (!fs.existsSync(targetFolder)){
    fs.mkdirSync(targetFolder, { recursive: true });
}

var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: false
});

let filePath = targetFolder+'/yarn-audit.json'
fs.truncate(filePath, 0, function(){console.log('file cleared')})

let array = []

rl.on('line', function(line){
    array.push(JSON.parse(line))
})

rl.on('close', function(){
    fs.appendFile(filePath, JSON.stringify(array, null, 2),
        function (err) {
            if (err) { console.error(err)}
        }
    )
})


