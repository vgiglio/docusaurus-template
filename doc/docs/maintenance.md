# Maintenance

Permet d'administrer la mise en maintenance de l'établissement.

Lorsque les services de la solution ENOVACOM Patient Link (EPL) de l'établissement risquent d'être interrompus de manière planifiée (ex: mise à jour serveur, coupure réseau prévue), la mise en maintenance de l'établissement permet de prévenir les utilisateurs de l'application patient "CARNET" que les services EPL de l'établissement ne sont pas disponibles le temps de la maintenance.

Une fois la maintenance désactivée, l'établissement est de nouveau "actif", tout comme les services EPL proposés aux utilisateurs de l'application patient "CARNET".

## Activer la maintenance 

Pour activer la maintenance de l'établissement, au clic sur "Activer la maintenance" les champs suivants sont demandés :

* Une date de début  (*obligatoire*) : date de début de la maintenance 
* Une heure de début  (*obligatoire*) : heure de début de la maintenance 
* Une date de fin  (*obligatoire*) : date de fin prévue de la maintenance 
* Une heure de fin  (*obligatoire*) : heure de fin prévue de la maintenance 
* Le message à afficher au format markdown (*obligatoire*)

Remarque : les dates et heures indiquées sont affichées à titre informatif aux utilisateurs de l'application patient "CARNET". Par exemple : si la date ou heure de fin de maintenance est passée mais que la maintenance est toujours active, alors l'établissement reste en maintenance aux yeux des utilisateurs de l'application patient "CARNET".

## Désactiver la maintenance 

Pour désactiver la maintenance de l'établissement, cliquer sur "Désactiver la maintenance" et confirmer.

L'établissement repasse alors au statut "Actif" et tous les services EPL sont à nouveau disponibles pour les utilisateurs de l'application patient "CARNET".

## Afficher le détail de la maintenance

Pour afficher le détail de la maintenance en cours et voir l'aperçu du message affiché aux utilisateurs de l'application patient "CARNET", cliquer sur "Voir les détails".