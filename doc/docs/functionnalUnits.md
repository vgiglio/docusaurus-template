---
id: functionnalUnits
title: Unités fonctionnelles
---

# Unités fonctionnelles

Permet de consulter la liste des unités fonctionnelles de l'établissement.

## Lister les unités fonctionnelles

Les unités fonctionnelles affichées correspondent aux ressources "Location" dans l'EDR (Enovacom Data Repository) de l'établissement.

## Afficher le détail d'une unité fonctionnelle

Pour afficher le détail d'une unité fonctionnelle, cliquer sur une carte d'unité fonctionnelle : les informations de l'unité fonctionnelle s'affichent en lecture seule.

Les informations affichées proviennent de la ressource "Organization" référencée par la ressource "Location" correspondante à l'unité fonctionnelle dans l'EDR de l'établissement.

## Rechercher une unité fonctionnelle

Pour rechercher une unité fonctionnelle dans la liste, saisir les caractères du nom de l'unité fonctionnelle dans la barre de recherche en haut de la liste : les unités fonctionnelles dont le nom contient les caractères saisis s'afficheront dynamiquement.
