# Consignes opératoires

Permet d'administrer les consignes opératoires des unités fonctionnelles de l'établissement.

Les consignes opératoires d'une unité fonctionnelle sont affichées dans le détail d'un rendez-vous qui a lieu dans cette unité fonctionnelle, dans l'application "CARNET" à destination des patients.

## Créer une consigne

Pour créer une consigne, cliquer sur le bouton (+) en bas de page.

Une consigne se définit par les informations suivantes : 

* Un type (*obligatoire*) : opératoire post-opératoire, pré-opératoire
* Une unité fonctionnelle associée
* Le détail de la consigne au format markdown (*obligatoire*)

## Editer une consigne

Pour éditer une consigne, cliquer sur le bouton "Editer" sur la consigne souhaitée.

Tous les champs sont éditables.

## Supprimer une consigne

Pour supprimer une consigne, cliquer sur le bouton "Supprimer" sur la consigne souhaitée.

Attention, après confirmation de suppression, cette action est irréversible.

## Afficher le détail d'une consigne

Pour afficher le détail d'une consigne, cliquer sur une carte de consigne  : le détail de la consigne s'affiche telle qu'elle apparaitra dans l'application patient.

## Rechercher une consigne

Pour rechercher une consigne dans la liste,  saisir les caractères du type de consigne dans la barre de recherche en haut de la liste : les consignes dont le type contient les caractères saisis s'afficheront dynamiquement.
