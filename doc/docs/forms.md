# Formulaires

Permet d'administrer les formulaires de préadmission de l'établissement.

Le formulaire de préadmission d'une unité fonctionnelle est proposé dans l'application patient "CARNET" au moment où ce dernier va remplir sa préadmission pour un rendez-vous dans l'unité fonctionnelle concernée.

## Créer un formulaire

Pour créer un formulaire, cliquer sur le bouton (+) en bas de page.

Un formulaire se définit par les informations suivantes : 

* Un titre (*obligatoire*)
* Un type (uniquement "Préadmission" pour le moment) (*obligatoire*)
* Une description (*obligatoire*)
* Un item : fichier json (au format brut pour le moment) qui représente la structure du formulaire de préadmission  (*obligatoire*)
* Une ou plusieurs unité(s) fonctionnelle(s) associée(s)  (*obligatoire*)

## Formulaire par défaut 

Par défaut, l'association d'un formulaire à au moins une unité fonctionnelle est obligatoire.

Il est néamoins possible de rendre ce champ facultatif, et donc de permettre d'avoir un formulaire "par défaut" dans l'établissement, qui s'appliquera pour toute unité fonctionnelle qui ne serait associée à aucun formulaire.

Pour permettre l'utilisation d'un formulaire par défaut, il faut activer la propriété suivante dans le fichier de configuration du serveur :

```conf
INSTITUTION_DEFAULT_FORM_AUTHORIZED="true"
```

## Editer un formulaire

Pour éditer un formulaire, cliquer sur le bouton "Editer" sur le formulaire souhaité.

Tous les champs sont éditables.

## Supprimer un formulaire

Pour supprimer un formulaire, cliquer sur le bouton "Supprimer" sur le formulaire souhaité.

Attention, après confirmation de suppression, cette action est irréversible.

## Afficher le détail d'un formulaire

Pour afficher le détail d'un formulaire, cliquer sur une carte de formulaire  : les informations du formulaire s'affichent en lecture seule.

## Rechercher un formulaire

Pour rechercher un formulaire dans la liste,  saisir les caractères du titre du formulaire dans la barre de recherche en haut de la liste : les formulaires dont le titre contient les caractères saisis s'afficheront dynamiquement.
