# Modèles de notifications

Permet d'administrer les modèles de notifications de l'établissement.

Les notifications (de type mail, sms et mobile) sont envoyées par l'établissement à destination des utilisateurs de l'application patient "CARNET" lors de l'envoi de rendez-vous, de document ou autre information.

## Modèles de notifications par défaut 

Par défaut, il existe un modèle de notification par défaut pour chaque type de notification et pour chaque évènement qui sera envoyé aux utilisateur de l'application patient "CARNET" : 

Type de notification :

* Mail
* sms
* Mobile

Evènement : 

* Demande de code d'appairage
* Demande de code d'authentification
* Nouveau document
* Nouvelle préadmission à remplir
* Préadmission invalidée
* Préadmission validée



## Créer un modèle de notification personnalisé

Pour créer un modèle de notification personnalisé, se positionner dans l'onglet du type de notification souhaité (mail, sms ou mobile) et cliquer sur le bouton (+) en bas de page.

Un modèle de notification se définit par les informations suivantes : 

* Un type (*obligatoire*) : parmi la liste des évènements disponibles
* Un objet (uniquement pour les types de notification mail et mobile) (*obligatoire*)
* Une langue (*obligatoire*)
* Un contenu (*obligatoire*)

### Modèle de mail

Le contenu d'un modèle de mail est au format HTML brut (pour le moment).

Il s'agit d'un format mjml transformé en HTML via un outil de génération mjml --> HTML par exemple.

### Modèle de sms et notification mobile

Le contenu d'un modèle de sms et notification mobile est au format brut.

Le contenu sera affiché tel qu'il apparait sur le sms ou la notification mobile (les emojis sont autorisés).

## Editer un modèle

Pour éditer un modèle de notification, cliquer sur le bouton "Editer" sur le modèle souhaité.

Tous les champs sont éditables.

## Supprimer un modèle

Pour supprimer un modèle de notification, cliquer sur le bouton "Supprimer" sur le modèle souhaité.

Attention, après confirmation de suppression, cette action est irréversible.

## Afficher le détail d'un modèle

Pour afficher le détail d'un modèle, cliquer sur une carte de formulaire  : les informations du modèle s'affichent en lecture seule.

## Rechercher un modèle

Pour rechercher un modèle dans la liste,  saisir les caractères du type ou de la langue du modèle dans la barre de recherche en haut de la liste : les modèles dont le type ou la langue contient les caractères saisis s'afficheront dynamiquement.