# Détails de l'établissement

Permet d'administrer le détail de l'établissement tel qu'il sera présenté dans l'application patient "CARNET" au moment où ce dernier va consulter le détail de l'établissement avec lequel il est "connecté".

## Description

Correspond à la description globale de l'établissement.

### Créer une description

Pour ajouter une description, cliquer sur le bouton (+) en bas de page :

* Nom  (*obligatoire*) 
* Adresse postale  (*obligatoire*)
* Coordonnées GPS en degrés décimaux
* Moyens de communication
* Réseaux sociaux

### Editer la description

Une fois la description ajoutée, il est possible de charger le logo de l'établissement qui est une image au format JPG, JPEG ou PNG dont la taille n'excède pas 1Mo.

Pour éditer la description, cliquer sur le bouton d'édition.

Tous les champs sont éditables.

### Supprimer la description

Pour supprimer un plan, cliquer sur le bouton de suppression.

Attention, après confirmation de suppression, cette action est irréversible.


## Plans

Correspond au(x) plan(s) de l'établissement, qui est (sont) affiché(s) sous la forme d'un carrousel dans l'application patient "CARNET" au moment où ce dernier va consulter le détail de l'établissement.

### Créer un plan

Pour ajouter un plan, cliquer sur le bouton (+) en bas de page :

* Nom  (*obligatoire*) 
* Pièce jointe  (*obligatoire*) : image au format JPG, JPEG ou PNG dont la taille n'excède pas 1Mo.

### Editer un plan

Pour éditer un plan, cliquer sur le bouton "Editer" sur le plan souhaité.

Tous les champs sont éditables.

### Supprimer un plan

Pour supprimer un plan, cliquer sur le bouton "Supprimer" sur le plan souhaité.

Attention, après confirmation de suppression, cette action est irréversible.

### Rechercher un plan

Pour rechercher un plan dans la liste,  saisir les caractères du titre du plan dans la barre de recherche en haut de la liste : les plans dont le titre contient les caractères saisis s'afficheront dynamiquement.

## Moyens d'accès

Correspond au(x) moyen(s) d'accès à l'établissement qu'on souhaite indiquer aux utilisateurs de l'application patient "CARNET".

### Créer un moyen d'accès

Pour ajouter un moyen d'accès, cliquer sur le bouton (+) en bas de page :

* Nom  (*obligatoire*) 
* Type  (*obligatoire*) : choix parmi la liste de valeurs proposée
* Description au format markdown

### Editer un moyen d'accès

Pour éditer un moyen d'accès, cliquer sur le bouton "Editer" sur le moyen d'accès souhaité.

Tous les champs sont éditables.

### Supprimer un moyen d'accès

Pour supprimer un moyen d'accès, cliquer sur le bouton "Supprimer" sur le moyen d'accès souhaité.

Attention, après confirmation de suppression, cette action est irréversible.

### Rechercher un moyen d'accès

Pour rechercher un moyen d'accès dans la liste,  saisir les caractères du titre ou du type du moyen d'accès dans la barre de recherche en haut de la liste : les moyens d'accès dont le titre ou le type contient les caractères saisis s'afficheront dynamiquement.

## Parkings

Correspond au(x) parking(s) proches de l'établissement qu'on souhaite indiquer aux utilisateurs de l'application patient "CARNET".

### Créer un parking

Pour ajouter un parking, cliquer sur le bouton (+) en bas de page :

* Nom  (*obligatoire*) 
* Description au format markdown
* Coordonnées GPS en degrés décimaux (*obligatoire*)  : permettront de faire apparaitre la localisation du parking sur une carte aux utilisateurs de l'application patient "CARNET"

### Editer un parking

Pour éditer un parking, cliquer sur le bouton "Editer" sur le parking souhaité.

Tous les champs sont éditables.

### Supprimer un parking

Pour supprimer un parking, cliquer sur le bouton "Supprimer" sur le parking souhaité.

Attention, après confirmation de suppression, cette action est irréversible.

### Rechercher un parking

Pour rechercher un parking dans la liste,  saisir les caractères du titre du parking dans la barre de recherche en haut de la liste : les parkings dont le titre contient les caractères saisis s'afficheront dynamiquement.

## Services

Correspond au(x) service(s) de l'établissement qu'on souhaite indiquer aux utilisateurs de l'application patient "CARNET" lorsqu'ils vont consulter le détail de l'établissement.

### Créer un service

Pour ajouter un service, cliquer sur le bouton (+) en bas de page :

* Nom  (*obligatoire*) 
* Contact : N° de téléphone du service

### Editer un service

Pour éditer un service, cliquer sur le bouton "Editer" sur le service souhaité.

Tous les champs sont éditables.

Il est également possible d'associer à l'édition d'un service un ou plusieurs moyen(s) d'accès et/ou parking(s), qui seront rattachés au service lorsque les utilisateurs de l'application patient "CARNET" vont consulter le détail de l'établissement.

#### Ajouter un moyen d'accès à un service

Pour ajouter un moyen d'accès à un service, cliquer sur le bouton "Ajouter" en édition du service : 

* Nom  (*obligatoire*) 
* Type  (*obligatoire*) : choix parmi la liste de valeurs proposée
* Description au format markdown

#### Editer un moyen d'accès associé à un service

Pour éditer un moyen d'accès associé à un service, cliquer sur le bouton d'édition à côté du moyen d'accès souhaité.

Tous les champs sont éditables.

#### Supprimer un moyen d'accès associé à un service

Pour supprimer un moyen d'accès associé à un service, cliquer sur le bouton de suppression à côté du moyen d'accès souhaité.

Attention, après confirmation de suppression, cette action est irréversible.

#### Ajouter un parking à un service

Pour ajouter un parking à un service, cliquer sur le bouton "Ajouter" en édition du service : 

* Nom  (*obligatoire*) 
* Description au format markdown
* Coordonnées GPS en degrés décimaux (*obligatoire*) 

#### Editer un parking associé à un service

Pour éditer un parking associé à un service, cliquer sur le bouton d'édition à côté du parking souhaité.

Tous les champs sont éditables.

#### Supprimer un parking associé à un service

Pour supprimer un parking, cliquer sur le bouton de suppression à côté du parking souhaité.

Attention, après confirmation de suppression, cette action est irréversible.

### Rechercher un service

Pour rechercher un service dans la liste,  saisir les caractères du nom du service dans la barre de recherche en haut de la liste : les services dont le nom contient les caractères saisis s'afficheront dynamiquement.
