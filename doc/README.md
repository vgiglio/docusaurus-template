# Template Docusaurus

Ce projet a été généré par `npx create-docusaurus`, puis personnalisé pour les besoins des applications Enovacom.

Il sert de template à recopier / forker pour ajouter une doc en ligne à votre application, dans un sous-répertoire de votre projet ou bien sous forme de projet à part entière.

Le projet étant amené à évoluer, toutes propositions d'améliorations sont les bienvenues. 🙂

## Exemple d'intégration dans un projet

- Pour intégrer ce template docusaurus, récupérez depuis le fichier pom.xml à la racine de ce projet tout le contenu qui est commenté y compris le module doc et ajouter le au pom.xml à la racine de votre propre projet.
- Ajouter ensuite le dossier doc à la racine de votre projet au même niveau que votre back et votre front puis faites en sorte de le builder en même temps que votre front. Pour terminer ajouter dans votre inteface utilisateur un lien de redirection vers votre docusaurus.

## Installation de la partie web

placez vous dans le dossier doc et depuis votre terminal lancez la commande:

```
yarn install
```

## Test rapide

### (fonctionnalitées: recherche et traduction non disponibles)

```
yarn start
```

## Test de toutes les fonctionnalitées

Afin de pouvoir utiliser toutes les fonctionnalitées comme la recherche et la traduction, un simple yarn start ne fonctionnera pas. Vous devrez builder l'application de cette manière:

```
yarn build
yarn run serve
```

Et ensuite vous rendre sur votre localhost

## Navigation avec la documentation et sideBar

Afin que la navigation dans la documentation soit correctement mise en place par docusaurus et le generateur de pdf, il est conseillé de ne pas créer d'en-tête dans les fichiers markdown et de mettre les informations de navigation dans le fichier [sidebar.js](/sidebars.js) en suivant un des [exemples](https://docusaurus.io/docs/sidebar#sidebar-object).

## Gestion des versions

Vous pouvez gérer les différentes versions de votre documentation
grâce à l'option

    navbar: {
        items: [
            {
              type: "docsVersionDropdown",
              position: "left",
              dropdownActiveClassDisabled: true,
            },
        ],
    },

dans le fichier [docusaurus.config.js](docusaurus.config.js).

Pour générer correctement les différentes versions de votre documentation, il faudra respecter certaines étapes qui sont décrites [ici](https://docusaurus.io/fr/docs/versioning#tutorials).

Très brievement il faudra créer un fichier version.json
avec un tableau vide.
ensuite en tapant depuis votre terminal la commande:  
(modifier le numéro de version en fonction de votre projet)

```
yarn run docusaurus docs:version 1.1.0
```

Un dossier versioned_docs et versioned_sidebars vont se créer automatiquement et récupérer respectivement le contenu de votre dossier [docs](docs/) et votre [sidebar.js](sidebars.js) actuel en les nommants avec le numéro de version indiqué dans la commande ci-dessus.

Vous pourrez donc ensuite modifier le contenu de votre doc et de votre sidebar.

Si vous souhaitez changer de version à afficher comme étant la dernière vous pouvez le faire depuis le fichier [docusaurus.config.js](docusaurus.config.js)

      presets: [
          [
            ({
              docs: {
                lastVersion: "current",
                versions: {
                  current: {
                    label: "1.0.0 (actuelle)",
                  },
                },
              },

vous trouverez plus d'informations [ici](https://docusaurus.io/fr/docs/versioning#configuring-versioning-behavior)

les traduction où il faut créer un dossier encore du meme nom au meme niveau que current dans doc\i18n\en\docusaurus-plugin-content-docs et le fichier du meme nom .json avec les traduction des variables et objet

## Génération des pdf

- les pdf sont générés grâce au pom.xml racine qui récupère les informations fournies dans le fichier [docs.json](docs.json) présent dans votre dossier doc. Il faudra donc modifier les informations du fichier par les votres en respectant l'architecture.

- En tapant depuis votre terminal la commande

```
mvn install
```

Celui-ci lance alors un script qui récupère les différentes informations du fichier [docs.json](docs.json) et génère les pdf dans toutes les langues, dans le répertoire [static/pdf/](static/pdf/)

- Possibilité de télécharger la documentation en format PDF depuis la page d'accueil en fonction de la langue et version sélectionnées

## Traduction

Pour créer et gérer les traductions de votre documentation
vous devrez les déclarer dans le fichier [docusaurus.config.js](docusaurus.config.js) en suivant cet exemple

      const config = {
    i18n: {
      defaultLocale: "fr",
      locales: ["fr", "en"],
      localeConfigs: {
        fr: {
          label: "Français",
        },
        en: {
          label: "English",
        },
        it: {
          label: "Italiano",
        },
      },
    },

Ensuite vous devrez créer les dossier suivant en respectant l'architecture depuis le dossier doc

    - i18n/
        - en/
            - docusaurus-plugin-content-docs/
                - current/
                - version-O.5.0/
                - current.json
                - version-0.5.0.json
        - it/

Puis pour la version en cours de votre documentation vous pourrez mettre les traductions dans le dossier current et respectivement vous pourrez mettre la documentation de vos anciennes versions de documentation dans le dossier correspondant comme dans l'exemple ci-dessus avec le dossier "version-0.5.0".

Pour terminer à l'intérieur des fichier current.json
et version-0.5.0.json vous pourrez y mettre les traduction des différentes variables utilisées dans docusaurus comme dans cet [exemple](i18n/en/docusaurus-plugin-content-docs/current.json).

Il est également de traduire d'autres variables en dehors de la documentation comme par exemple le texte de la barre de navigation en suivant le meme principe qu'au dessus depuis ce fichier [navbar.json](doc\i18n\en\docusaurus-theme-classic\navbar.json) ou encore celui-ci [code.json](doc\i18n\en\docusaurus-theme-classic\code.json).

Pour plus de détails sur les traductions vous pouvez vous rendre [ici](https://docusaurus.io/fr/docs/i18n/introduction#translation-workflow).

## Audit des libs web

```
yarn eno-audit
```

## Customisation / css

[lien infima](https://infima.dev/docs/getting-started/introduction)

## TODO

- Maven yarn build
- Lors d'une release, copie automatique des pdf dans un repo commun, afin de centraliser les docs de toutes les applis, pour toutes les versions
- Téléchargement du zip des pdf via l'ihm (déjà commencé, à reprendre)
- ...
